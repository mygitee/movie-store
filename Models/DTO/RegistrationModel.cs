﻿using System.ComponentModel.DataAnnotations;

namespace MovieStore.Models.DTO
{
    public class RegistrationModel
    {
        [Required]
        public required string Name { get; set; }
        [Required]
        [EmailAddress]
        public required string Email { get; set; }
        public required string Username { get; set; }

        [Required]
        [RegularExpression("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*[#$^+=!*()@%&]).{6,}$", ErrorMessage = "Minimum length 6 and must contain  1 Uppercase,1 lowercase, 1 special character and 1 digit")]
        public required string Password { get; set; }
        [Required]
        [Compare("Password")]
        public required string PasswordConfirm { get; set; }
        public required string Role { get; set; }
    }
}
