﻿using Microsoft.AspNetCore.Identity;

namespace MovieStore.Models.Domain
{
    public class ApplicationUser:IdentityUser
    {
        public required string Name { get; set; }
    }
}
