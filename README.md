# MovieStore

#### 介绍
在asp.net中构建MVC项目使用身份进行授权，并提供管理员登录部分，适合学习练手。

#### 使用说明
1. 连接本地sqlserver数据库
2. 打开UserAuthenticationController -->Register注释，创建默认admin账户

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request