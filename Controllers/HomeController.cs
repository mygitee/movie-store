﻿using Microsoft.AspNetCore.Mvc;
using MovieStore.Repositories.Abstract;
using MovieStore.Repositories.Implementation;

namespace MovieStore.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMovieService movieService;
        public HomeController(IMovieService movieService)
        {
            this.movieService = movieService;
        }

        public IActionResult Index(string term = "", int currentPage = 1)
        {
            var movies = movieService.List(term, true, currentPage);
            return View(movies);
        }

        public IActionResult About()
        {
            return View();
        }
        public IActionResult MovieDetail(int movieId)
        {
            var movie = movieService.GetById(movieId);
            return View(movie);
        }
    }
}
