﻿using Microsoft.AspNetCore.Mvc;
using MovieStore.Models.DTO;
using MovieStore.Repositories.Abstract;

namespace MovieStore.Controllers
{
    public class UserAuthenticationController : Controller
    {
        private IUserAuthenticationService authService;

        public UserAuthenticationController(IUserAuthenticationService authorService)
        {
            this.authService = authorService;
        }
        /// <summary>
        /// 创建用户
        /// </summary>
        /// <returns></returns>
        //public async Task<IActionResult> Register()
        //{
        //    var model = new RegistrationModel
        //    {
        //        Email="admin@gamil.com",
        //        Username = "admin",
        //        Password = "Admin@123",
        //        PasswordConfirm = "Admin@123",
        //        Name="十一",
        //        Role = "admin"
        //    };
        //    var result = await authService.RegisterAsync(model);
        //    return Ok(result.Message);
        //}
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var result = await authService.LoginAsync(model);
            if (result.StatusCode == 1)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                TempData["msg"] = "登录失败";
                return RedirectToAction(nameof(Login));
            }

        }

        public async Task<IActionResult> LogOut()
        {
            await authService.LogoutAsync();
            return RedirectToAction(nameof(Login));
        }
    }
}
